/* ---------- TEAM ----- ---------- */
CEO: Aaron Crandall
Site: http://behaviometrics.io
Twitter: na
Location: Pullman, Washington, USA

CTO: Brian Thomas
Site: http://behaviometrics.io
Twitter: na
Location: Pullman, Washington USA

COO: Krishnan Gopalan
Site: http://behaviometrics.io
Twitter: na
Location: Pullman, Washington USA

Developer: Andrew Bates
Site: http://andrewbates09.com
Twitter: https://twitter.com/andrewbates09
Location: Moscow, Idaho, USA

/* ---------- THANKS --- ---------- */

Name: Artificial Intelligence Laboratory
Site: https://ailab.wsu.edu

Name: CASAS
Site: http://casas.wsu.edu

Name: Washington State University
Site: https://wsu.edu


/* ---------- SITE ----- ---------- */
Last update: 2015/11/08
Language: English
Doctype: HTML5
IDE: VIM
Source: http://behaviometrics.io
Standards: HTML5, CSS3
Components: none
Software: none
